package com.yalla.selenium.api.base;

import org.testng.annotations.Test;

import utils.Datalibrary;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.*;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class Annotations extends SeleniumBase{
	@DataProvider(name ="fetchData")
	public Object[][] fetchData() throws IOException 
	{
		return Datalibrary.readexcel(filename);
	}
	
	
  
	//@Parameters({"url", "username", "password"})  
	//@Parameters({"url", "username", "password"})
  //@BeforeMethod(groups = "any")
	//@BeforeMethod
/*  public void beforeMethod(String url,String username,String password) {
	  
	  startApp("Chrome", url);*/
	//public void beforeMethod()
	{
		//startApp ("Chrome","http://leaftaps.com/opentaps");
		// Login page User name and Password
		/*WebElement eleUserName = locateElement("id", "username");
		clearAndType(eleUserName, username);
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, password);
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		
		// Click CRM/SFA Link
		WebElement eleLink = locateElement("link", "CRM/SFA");
		click(eleLink);*/
  }
	@BeforeMethod
	public void beforeMethod() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		clearAndType(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleLink = locateElement("link", "CRM/SFA");
		click(eleLink);
		
	}
  @AfterMethod
  public void afterMethod() {
  }

  @BeforeClass
  public void beforeClass() {
  }

  @AfterClass
  public void afterClass() {
  }

  @BeforeTest
  public void beforeTest() {
  }

  @AfterTest
  public void afterTest() {
  }

  @BeforeSuite
  public void beforeSuite() {
  }

  @AfterSuite
  public void afterSuite() {
  }

}
