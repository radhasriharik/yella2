package com.yalla.selenium.api.base;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.sound.midi.SysexMessage;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.internal.Systematiser;

import com.yalla.selenium.api.design.Browser;
import com.yalla.selenium.api.design.Element;

import Reports.Basicreports;


public class SeleniumBase extends Basicreports implements Browser, Element{

	public RemoteWebDriver driver;
	WebDriverWait wait;
	int i=1;
	
	
	public void reportStep(String dec, String status) {
    	if(status.equalsIgnoreCase("pass")) {
    		test.pass(dec);
    	} else if(status.equalsIgnoreCase("fail")) {
    		test.fail(dec); 
    	}
    }
	@Override
	public void click(WebElement ele) {
		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));
			ele.click();
			reportStep("The Element "+ele+" clicked", "pass"); 
			//System.out.println("The Element "+ele+" clicked");
		} catch (StaleElementReferenceException e) {
			reportStep("The Element "+ele+" could not be clicked", "fail");
			//System.err.println("The Element "+ele+" could not be clicked");
			throw new RuntimeException();
		} finally { 
			//takeSnap();
		}
	}
	@Override
	public void append(WebElement ele, String data) {
		String test = ele.getText();
		test.concat(data);
		
		
		// TODO Auto-generated method stub
	

	}

	@Override
	public void clear(WebElement ele) {
		try {
			ele.clear();
			reportStep("The Element "+ele+" cleared", "pass"); 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			reportStep("The Element "+ele+" could not be cleared", "fail");
			//System.err.println("The element is not cleared");
		}
		// TODO Auto-generated method stub

	}

	@Override
	public void clearAndType(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data);
			reportStep("The Element "+ele+" Cleared", "pass"); 
			//System.out.println("The Data :"+data+" entered Successfully");
		} catch (ElementNotInteractableException e) {
			reportStep("The Element "+ele+" could not be cleared", "fail");
			//System.err.println("The Element "+ele+" is not Interactable");
			throw new RuntimeException();
		}

	}

	@Override
	public String getElementText(WebElement ele) {
		// TODO Auto-generated method stub
		try {
			ele.getText();
			reportStep("The Element "+ele+" getElement text", "pass"); 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			reportStep("The Element "+ele+" Not able to find gettext", "fail");
			//System.err.println("Not able to find gettext");
		}
		return null;
	}

	@Override
	public String getBackgroundColor(WebElement ele) {
		try {
			ele.getCssValue("value");
			reportStep("The Element "+ele+" getBackgroundColor", "pass"); 
		} catch (Exception e) {
			reportStep("The Element "+ele+" could not be not find backgroundColor", "fail");
			// TODO Auto-generated catch block
			
		}
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTypedText(WebElement ele) {
		
		ele.getAttribute("value");
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		
		 
		   try {
			Select dropdown =new Select(ele);
			   dropdown.selectByVisibleText(value);
			   reportStep("The Element "+ele+" dropdown text is displayed", "pass"); 	   
		} catch (Exception e) {
			// TODO Auto-generated catch block
			reportStep("The Element "+ele+" Dropdown text value is mismatched", "fail");
			//System.err.println("Dropdown text value is mismatched");
		}
			
		// TODO Auto-generated method stub

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub
		 try {
			Select dropdown =new Select(ele);
			   dropdown.selectByIndex(4);
			   reportStep("The Element "+ele+" dropdown index is displayed", "pass");
		} catch (Exception e) {
			reportStep("The Element "+ele+" dropdown index is not displayed", "fail");
			// TODO Auto-generated catch block
		//System.err.println("Index value is mismatched");
		}
	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		// TODO Auto-generated method stub
		try {
			Select dropdown =new Select(ele);
			   dropdown.selectByValue(value);
			   reportStep("The Element "+ele+" dropdown value is displayed", "pass");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			reportStep("The Element "+ele+" dropdown value is not displayed", "fail");
			//System.err.println("String Value is mismatched");
		
		}

	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
		
		
		String test=ele.getText();
		if(test.equals(expectedText))
		{
			System.out.println("The text "+test+" is matched");
			return true;
		}
		else
		{
			System.out.println("The text "+test+" is not matched");
		}
		return false;
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		String text = ele.getText();
		if (text.contains(expectedText)) {
			System.out.println("The text "+text+" is matched");
			return true;
		} else {
			System.out.println("The text "+text+" is not matched");
		}
		return false;
	}

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		String test=ele.getAttribute(value);
		if(test.equals(attribute))
		{
			System.out.println("The text "+test+" attribute value is matched");
			return true;
		}
		else 
		{
			System.out.println("The text "+test+" attribute value is not matched");
		}
		
		return false;
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		{
			String test=ele.getAttribute(value);
			if(test.contains(attribute))
			{
				System.out.println("The text "+test+" attribute value is matched");
				
			}
			else 
			{
				System.out.println("The text "+test+" attribute value is not matched");
			}
			
		}
		}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
		if(ele.isDisplayed())
		{
			System.out.println("element is displayed");
            return true;
		}
            else
          {
              System.out.println("element not displayed");
           return false;
          }
     }
    

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		if(!ele.isDisplayed())
		{
			System.out.println("element is not displayed");
            return true;
		}
            else
          {
              System.out.println("element is displayed");
          }

		return false;
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {
		
		if(ele.isEnabled())
		{
			System.out.println("element is enabled");
            return true;
		}
            else
          {
              System.out.println("element is not enabled");
          }
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifySelected(WebElement ele) {
		
		if(ele.isSelected())
		{
			System.out.println("element is selected");
            return true;
		}
            else
          {
              System.out.println("element is not selected");
          }
		// TODO Auto-generated method stub
		return false;
	}
		// TODO Auto-generated method stub

	@Override
	public void startApp(String url) {
		 {
			 driver = new ChromeDriver();
			 System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
				driver.manage().window().maximize();
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.navigate().to(url);
		 }
			
		// TODO Auto-generated method stub

	}

	@Override
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver",
						"./drivers/chromedriver.exe");
				driver = new ChromeDriver();
				/*ChromeOptions op = new ChromeOptions();
				op.addArguments("-disable-notifications");
				driver = new ChromeDriver(op);*/
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver",
						"./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			} else if(browser.equalsIgnoreCase("ie")) {
				System.setProperty("webdriver.ie.driver",
						"./drivers/IEDriverServer.exe");
				driver = new InternetExplorerDriver();
			}
			driver.navigate().to(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.err.println("The Browser Could not be Launched. Hence Failed");
			throw new RuntimeException();
		}

	}

	@Override
	public WebElement locateElement(String locatorType, String value) {
		try {
			switch(locatorType.toLowerCase()) {
			case "id": return driver.findElementById(value);
			case "name": return driver.findElementByName(value);
			case "class": return driver.findElementByClassName(value);
			case "link": return driver.findElementByLinkText(value);
			case "xpath": return driver.findElementByXPath(value);
			}
		} catch (NoSuchElementException e) {
			System.err.println("The Element with locator:"+locatorType+" Not Found with value: "+value);
			throw new RuntimeException();
		}
		return null;
	}

	@Override
	public WebElement locateElement(String value) 
	{
		WebElement id = driver.findElementById(value);
		// TODO Auto-generated method stub
		
		return null;
	}

	
	@Override
	public List<WebElement> locateElements(String type, String value) {
		return null;
		
		
	}

	@Override
	public void switchToAlert() {
		try {
			driver.switchTo().alert();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.err.println("Alert is not displayed");
		}
		// TODO Auto-generated method stub

	}

	@Override
	public void acceptAlert() {
		try {
			driver.switchTo().alert().accept();
		} catch(Exception e)
		{
			System.err.println("Alert is not accepted");
		}
		
		// TODO Auto-generated method stub

	}

	@Override
	public void dismissAlert() {
		try
		{
			driver.switchTo().alert().dismiss();	
		}
		catch(Exception e)
		{
			System.err.println("Alert is not cancelled");
		}
		
		// TODO Auto-generated method stub

	}

	@Override
	public String getAlertText() {
		driver.switchTo().alert().getText();
		
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void typeAlert(String data) {
		driver.switchTo().alert().sendKeys(data);
		
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToWindow(int index) {
		
		Set<String> test = driver.getWindowHandles();
		List<String> lab=new ArrayList();
		lab.addAll(test);
		driver.switchTo().window(lab.get(index));
		
	}

	@Override
	public void switchToWindow(String title) {
		// TODO Auto-generated method stub
		
		Set<String> test = driver.getWindowHandles();
		List<String> lab=new ArrayList();
		driver.switchTo().window(title);
	}

	@Override
	public void switchToFrame(int index) {
		driver.switchTo().frame(index);
		
	}

	@Override
	public void switchToFrame(WebElement ele) {
		driver.switchTo().frame(ele);
	}

	@Override
	public void switchToFrame(String idOrName) {
		driver.switchTo().frame(idOrName);
		// TODO Auto-generated method stub

	}

	@Override
	public void defaultContent() {
		driver.switchTo().defaultContent();
		// TODO Auto-generated method stub

	}

	@Override
	public boolean verifyUrl(String url) {
		String expectedresults = driver.getCurrentUrl();
		String actualresults="";
		if(expectedresults.equalsIgnoreCase(actualresults))
		{
			System.out.println("The URL is matched with expected results");
			return true;
		}
		else
		{
			System.out.println("The URL is not matched with expected reults");
		}

		return false;
	}

	@Override
	public boolean verifyTitle(String title) {
		// TODO Auto-generated method stub
		String expectedtitle = driver.getTitle();
		String actualtitle="";
		if(expectedtitle.equals(actualtitle))
				{
			System.out.println("The title is matched with the expected results");
			return true;
				}
		else
		{
			System.out.println("The title is not macthed with the expected results");
		}
		return false;
	}

	@Override
	public void takeSnap() {
		try {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File desc=new File("./snaps/snap"+i+".png");
		
			FileUtils.copyFile(src, desc);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("the snapot coult not be taken");
		}
		
		i++;	
	}

	@Override
	public void close() {
		driver.close();
		// TODO Auto-generated method stub

	}

	@Override
	public void quit() {
		driver.quit();
		// TODO Auto-generated method stub

	}

}
