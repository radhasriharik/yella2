package Reports;


		import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
		import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Basicreports {
	static ExtentHtmlReporter reporter;
	static ExtentReports extent;
	public ExtentTest test;
	public String testcaseName, testcaseDec, author, category,filename;
	@BeforeSuite(groups="any")
	public void startReport() {
		reporter = new ExtentHtmlReporter("./reports/result.html");
		reporter.setAppendExisting(true); 
		extent   = new ExtentReports();
		extent.attachReporter(reporter);
	}
    @BeforeMethod(groups= "any")
	public void report() throws IOException {
		test = extent.createTest(testcaseName, testcaseDec);
	    test.assignAuthor(author);
	    test.assignCategory(category);  
	}
    @BeforeClass(groups= "any")
    public void reportStep(String dec, String status) {
    	if(status.equalsIgnoreCase("pass")) {
    		test.pass(dec);
    	} else if(status.equalsIgnoreCase("fail")) {
    		test.fail(dec); 
    	}
    }
    @AfterSuite(groups= "any")
    public void stopReport() {
    	extent.flush();
    }
}
	
	
	
	


