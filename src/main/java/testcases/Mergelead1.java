package testcases;

import java.util.Scanner;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class Mergelead1 extends Login  //Login extends sele base
{
 
	@Test(groups = "regression")
//	@Ignore
//	@Test(enabled=false)
	public void MergeLeadTest() throws InterruptedException {
		System.out.println("MERGE LEAD ");
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter names to merge - ");
		String fn1 = sc.nextLine();
		String fn2 = sc.nextLine();
		sc.close();
		login();
		WebElement crmsfaLink = locateElement("link", "CRM/SFA");
		click(crmsfaLink);
		WebElement leadsTab = locateElement("link", "Leads");
		click(leadsTab);
		WebElement eleMergeBtn = locateElement("link", "Merge Leads");
		click(eleMergeBtn);
		WebElement fromBtn = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[1]");
		click(fromBtn);
		switchToWindow(1);
		WebElement firstName1 = locateElement("xpath","//input[@name='firstName']");
		clearAndType(firstName1, fn1);
		WebElement findLeadsBtn = locateElement("xpath","//button[text()='Find Leads']");
		click(findLeadsBtn);
		Thread.sleep(3000);
		WebElement firstResult = locateElement("xpath","(//table[@class='x-grid3-row-table']//a)[1]");
		String no1 = firstResult.getText();
		click(firstResult);
		switchToWindow(0);
		WebElement toBtn = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[2]");
		click(toBtn);
		switchToWindow(1);
		WebElement firstname2 = locateElement("xpath","//input[@name='firstName']");
		clearAndType(firstname2, fn2);
		takeSnap();
		WebElement findLeadsBtnto = locateElement("xpath","//button[text()='Find Leads']");
		click(findLeadsBtnto);
		Thread.sleep(3000);
		WebElement firstResultTo = locateElement("xpath","(//table[@class='x-grid3-row-table']//a)[1]");
		String no2 = firstResultTo.getText();
		click(firstResultTo);
		switchToWindow(0);
		WebElement mergebtn = locateElement("link","Merge");
		click(mergebtn);
		Thread.sleep(3000);
		switchToAlert();
		acceptAlert(); 
		WebElement findLeadTab = locateElement("link","Find Leads");
		click(findLeadTab);
		Thread.sleep(3000);
		WebElement findLeadFN = locateElement("xpath", "//input[@name='id']");
		clearAndType(findLeadFN, no1);
		WebElement btnFindLeads = locateElement("xpath","//button[text()='Find Leads']");
		click(btnFindLeads);
		Thread.sleep(3000);
		WebElement firstFindResult = locateElement("xpath","//div[@class='x-paging-info']");
		verifyExactText(firstFindResult, "No records to display");
		Thread.sleep(3000);
		takeSnap();
		
		
		
		
		
		
		
	}

	
	
}



