package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.Annotations;


public class Mergelead extends Annotations{
	
	

	@BeforeClass
	
		
		public void setData()
		{
			testcaseName = "Merge Lead";
			testcaseDec = "create a merge lead";
			author="radha";
			category="smoke";
			
		}

	
	@Test
		public void mergelead() throws InterruptedException
		{
			// Click Leads tab
			WebElement eleLeadstab =locateElement ("link","Leads");
			eleLeadstab.click();
			//Click on Merge Lead
			WebElement elemerge = locateElement ("link", "Merge Leads");
		    click(elemerge);
		    Thread.sleep(3000);
		    //Click on Find Lead icon(button)
		    WebElement elefindleadicon = locateElement("xpath","//img[contains(@src,'fieldlookup')][1]");
		    click(elefindleadicon);
		    //move to new window findleads
		    switchToWindow(1);
		    ////enter lead id first name
			WebElement firstName1 = locateElement("xpath","//input[@name='firstName']");
			clearAndType(firstName1, "radha");
			//click on find lead button
			WebElement findLeadsBtn = locateElement("xpath","//button[text()='Find Leads']");
			click(findLeadsBtn);
			Thread.sleep(3000);
			//click first resulting lead
			WebElement firstResult = locateElement("xpath","(//table[@class='x-grid3-row-table']//a)[1]");
			String no1 = firstResult.getText();
			click(firstResult);
			//move back to primary window
			switchToWindow(0);
			//click on to lead icon
			WebElement eleToleadicon = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[2]");
			click(eleToleadicon);
			// move to window
			switchToWindow(1);
			//enter to lead firstname
			WebElement firstname2 = locateElement("xpath","//input[@name='firstName']");
			clearAndType(firstname2, "senthil");
			takeSnap();
			//click on find lead button
			WebElement findLeadsBtn1 = locateElement("xpath","//button[text()='Find Leads']");
			click(findLeadsBtn1);
			Thread.sleep(3000);
			//click on first resulting lead
			WebElement firstResultTo = locateElement("xpath","(//table[@class='x-grid3-row-table']//a)[1]");
			String no2 = firstResultTo.getText();
			click(firstResultTo);
			//move again to window
			switchToWindow(0);
			//click on merge icon
			WebElement mergebtn = locateElement("link","Merge");
			click(mergebtn);
			Thread.sleep(3000);
			//alert pop up
			switchToAlert();
			//accept alert message
			acceptAlert(); 
			//clcik find lead tab
			WebElement findLeadTab = locateElement("link","Find Leads");
			click(findLeadTab);
			Thread.sleep(3000);
			//enter from lead
			WebElement findLeadFN = locateElement("xpath", "//input[@name='id']");
			clearAndType(findLeadFN, no1);
			//click find lead button
			WebElement btnFindLeads = locateElement("xpath","//button[text()='Find Leads']");
			click(btnFindLeads);
			Thread.sleep(3000); 
			//verify message
			WebElement firstFindResult = locateElement("xpath","//div[@class='x-paging-info']");
			verifyExactText(firstFindResult, "No records to display");
			Thread.sleep(3000);
			takeSnap();
			
		}
}

	
	
//WebElement viewFirstName = locateElement("id", "viewLead_firstName_sp");
//String firstName = getElementText(viewFirstName);
	

		    
		    
		    
		    
		    
		    
		    
			

	


