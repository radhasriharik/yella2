package testcases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.Annotations;




public class Duplicatelead extends Annotations{

	@BeforeClass
	

	public void setData()
	{
		testcaseName = "duplicate Lead";
		testcaseDec = "create a duplicate lead";
		author="radha";
		category="smoke";
	}
	
	
		@Test
		public void duplicatlead() throws InterruptedException
		{
			// Click Leads tab
			WebElement eleLeadstab =locateElement ("link","Leads");
			eleLeadstab.click();
			
            //Click findleads tab
			WebElement eleFindLeads = locateElement("link", "Find Leads");
			click(eleFindLeads);
					
			//Click on Email
			WebElement eleEmail = locateElement("xpath", "(//span[@class='x-tab-strip-text '])[3]");
			click(eleEmail);	
			
			//Enter Email
			WebElement eleEmailId = locateElement("name","emailAddress");
			clearAndType(eleEmailId,"test@test.com");
			
			//click find leads button
			WebElement findLeadsBtn = locateElement("xpath","//button[text()='Find Leads']");
			click(findLeadsBtn);
			Thread.sleep(3000);
			
			//Capture name of First Resulting lead
			WebElement firstLeadID = locateElement("xpath", "(//a[@class='linktext'])[4]");
			String textfirstLeadID = getElementText(firstLeadID);
			
			
			
			//Click First Resulting lead
		    WebElement firstLeadID1 = locateElement("link", "11679");
		    click(firstLeadID1);
			
					
			//Click Duplicate Lead
			WebElement duplicateLead = locateElement("link", "Duplicate Lead");
			click(duplicateLead);
			Thread.sleep(5000);
					
			//Verify the title as 'Duplicate Lead'
			verifyTitle("Duplicate Lead");
					
			//Click Create Lead
			WebElement createLead = locateElement("xpath", "//input[@value='Create Lead']"); 
			click(createLead);
			
			//Confirm the duplicated lead name is same as captured name
			WebElement firstNameDuplicate = locateElement("id", "viewLead_firstName_sp");
			verifyExactText(firstNameDuplicate,"test");
					
		}


		
			
		}


		
		
























		
		
