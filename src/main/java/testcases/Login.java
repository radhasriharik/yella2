package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.yalla.selenium.api.base.SeleniumBase;

public class Login extends SeleniumBase{
	@Test
	public void login() {
		startApp("ie", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		clearAndType(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		// Click CRM/SFA Link
					WebElement eleLink = locateElement("link", "CRM/SFA");
					click(eleLink);
					// Click Leads tab
					WebElement eleLeadstab = locateElement("link","Leads");
					eleLeadstab.click();
					// Click Create Lead Section
					WebElement eleCreatleadLink = locateElement("link", "Create Lead");
					click(eleCreatleadLink);
					// Enter Company Name
					WebElement eleCompanyName = locateElement("id", "createLeadForm_companyName");
					clearAndType(eleCompanyName,"Indium Testing");
					// Enter First Name
					WebElement eleFirstName = locateElement("id","createLeadForm_firstName");
					clearAndType(eleFirstName,"sriram444");
					// Enter Last Name
					WebElement eleLastName = locateElement("id","createLeadForm_lastName");
					clearAndType(eleLastName,"Gokul23");
					// dropdown selection for Source field using index
					WebElement ele2 = locateElement("id", "createLeadForm_dataSourceId");
					selectDropDownUsingIndex(ele2,4);
					// dropdown selection for Industry field using attribute value
					WebElement ele3 = locateElement("id", "createLeadForm_industryEnumId");
					selectDropDownUsingValue(ele3,"IND_TELECOM");
					// dropdown selection for ownership field using visible text
					WebElement ele = locateElement("id", "createLeadForm_ownershipEnumId");
					selectDropDownUsingText(ele,"Corporation");
					// Creating a Lead using create lead button
					WebElement eleCreateLeadButton = locateElement("xpath","//input[@value='Create Lead']");
					eleCreateLeadButton.click();
					// Find Leads
					WebElement eleFindleads = locateElement("link","Find Leads");
					eleFindleads.click();
					// Enter the First Name
					WebElement eleFirstNmae = locateElement("xpath","(//input[@name='firstName'])[3]");
					clearAndType(eleFirstNmae,"sriram");
					// Click Find Leads Button
					WebElement eleFindLeadButton = locateElement("xpath","//button[text()='Find Leads']");
					eleFindLeadButton.click();
					// Click First Leading Results 
					WebElement eleresults = locateElement("link","10330");
					eleresults.click();
					// Edit the lead
					WebElement eleEditelement = locateElement("link","Edit");
					eleEditelement.click();
					//Update the text
					WebElement eleUpdate = locateElement("id","updateLeadForm_companyName");
					clearAndType(eleUpdate,"Test Leaf World");
					//Click Update button
					WebElement eleupdatebutton = locateElement("xpath","//input[@name='submitButton']");
					eleupdatebutton.click();
					// updated text using gettext
					WebElement eleupdateresults = locateElement("id","viewLead_companyName_sp");
					String modifiedresult = eleupdateresults.getText();
					System.out.println(modifiedresult);
					// company name is modified or not
					if(eleCompanyName.equals(modifiedresult))
					{
						System.out.println("Company Name is not modified successfully");
					}
					else
					{
						System.out.println("Company Name is modified successfully");
					}
					// Duplicate Lead button
					WebElement eleDuplicate = locateElement("link","Duplicate Lead");
					eleDuplicate.click();
					//Create Duplicate lead
					WebElement eleDuplicatelead = locateElement("xpath","//input[@value='Create Lead']");
					eleDuplicatelead.click();
					//Verify Duplicate name is created
					WebElement getDuplicatetext = locateElement("id","viewLead_companyName_sp");
					String Duplicateresult = getDuplicatetext.getText();
					System.out.println(Duplicateresult);
					if(Duplicateresult.equals(Duplicateresult))
							{
						System.out.println("Duplicate is created successfully");
							}
					else
					{
						System.out.println("Duplicate is not created successfully");
					}
					// Merge Lead
					WebElement eleMerge = locateElement("link","Merge Leads");
					eleMerge.click();
					// Click the First icon
					WebElement eleFristicon = locateElement("xpath","//img[@alt='Lookup']");
					eleFristicon.click();
					// Control goes to the first window
					switchToWindow(1);
					System.out.println(driver.getTitle());
					WebElement eleleadID = locateElement("xapth","//input[@name='firstName']");
					clearAndType(eleleadID,"10330");
					WebElement eleFindLeadbuttons = locateElement("xapth","//button[text()='Find Leads']");
					eleFindLeadbuttons.click();
			
				}
			
		
		
	}
	








