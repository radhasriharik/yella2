package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.Annotations;
import com.yalla.selenium.api.base.SeleniumBase;

public class EditLead extends Annotations{


	//@BeforeClass
	@BeforeTest(groups="sanity")
	public void SetData()
	{
		testcaseName="EditLead";
		testcaseDec="Update a lead";
		author= "sriram";
		category="smoke";
	}	
	//@Test(dependsOnMethods={"testcases.Createlead.createlead"}, timeOut=10000)
	@Test(groups="sanity",dependsOnGroups="smoke")
	public void editLead()
	{
		//Click Leads tab
		WebElement eleLeadstab = locateElement("link","Leads");
		eleLeadstab.click();
		// Find Leads
		WebElement eleFindleads = locateElement("link","Find Leads");
		eleFindleads.click();
		WebElement eleEditID = locateElement("xpath","//input[@name='id']");
		clearAndType(eleEditID,"10126");
		WebElement eleleads22 = locateElement("xpath", "//button[text()='Find Leads']");
		eleleads22.click();
		// Edit the lead
		WebElement eleEditelement = locateElement("link","Edit");
		eleEditelement.click();
		//Update the text
		WebElement eleUpdate = locateElement("id","updateLeadForm_companyName");
		String cmpName=eleUpdate.getText();
		clearAndType(eleUpdate,"Test Leaf World");
		//Click Update button
		WebElement eleupdatebutton = locateElement("xpath","//input[@name='submitButton']");
		eleupdatebutton.click();
		// updated text using get text
		WebElement eleupdateresults = locateElement("id","viewLead_companyName_sp");
		String modifiedresult = eleupdateresults.getText();
		System.out.println(modifiedresult);
		// company name is modified or not

		if(cmpName.equals(modifiedresult))
		{
			System.out.println("Company Name is not modified successfully");
		}
		else
		{
			System.out.println("Company Name is modified successfully");
		}

	}

}