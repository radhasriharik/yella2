package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.Annotations;

public class Facebook extends Annotations{

@BeforeClass
	
	public void setData()
	{
		testcaseName = "Facebook";
		testcaseDec = "Login to Facebook";
		author="radha";
		category="smoke";
	}
		
	@Test
		public void facebook()
		{
		// URL Navigation
		startApp("Chrome", "https://www.facebook.com");
		// Login page User name and Password
		WebElement email = locateElement("id", "email");
		clearAndType(email, "vinubabu34@yahoo.co.in");
		WebElement elePassword = locateElement("id", "pass");
		clearAndType(elePassword, "godslovecare2128@86");
		WebElement eleLogin = locateElement("xpath","//input[@value ='Log In']");
		click(eleLogin);
		// click and search
		WebElement eleSearch = locateElement("xpath","//input[@placeholder='Search']");
		//enter the text test leaf
		clearAndType(eleSearch,"Testleaf");
		//click on search icon for searching
		WebElement eleSearch1 = locateElement("xpath","//button[@type='submit']");
		click(eleSearch1);
		//in dropdown select test leaf
		WebElement elePlaces = locateElement("xpath", "(//div[text()='Places'])[1]");
		click(elePlaces);
		//to capture the text of like button
		WebElement eleLikeTestLeaf = locateElement("xpath", "(//a[@href='https://www.facebook.com/TestleafSolutionsIncChennai/']/preceding::button[@type='submit'])[3]");
		String eleLikeText = getElementText(eleLikeTestLeaf);
		if(eleLikeText.equalsIgnoreCase("Like")) {		
			click(eleLikeTestLeaf);
		}
		
		//Click on like button
		WebElement elelike1 = locateElement("xpath","//button[@type='submit']");
		click(elelike1);
		//click on link testleaf
		//WebElement elelink1 = locateElement("xpath","//div[text()='TestLeaf']");
		//elelink1.click();
		//Verify Title contains test leaf
		verifyTitle("TestLeaf");
		//Capture number of likes for the page
		WebElement elelike2 = locateElement("xpath","//div[text()='7,065 people like this']");
		String likeText = getElementText(elelike2);
		String[] split = likeText.split(" ");
		String NoOfLikes=split[0];
		System.out.println("No of likes="+NoOfLikes);
		}
		}

	
		
		
		

	


