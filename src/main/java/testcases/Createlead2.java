



package testcases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.Annotations;

import utils.Datalibrary;

public class Createlead2 extends Annotations
{

	//@BeforeClass
	@BeforeTest(groups="smoke")

	public void setData()
	{
		testcaseName = "CreateLead";
		testcaseDec = "create a new lead";
		author="radha";
		category="smoke";
		
	}
	@DataProvider(name ="fetchData")
	public Object[][] fetchData() throws IOException 
	{
		return Datalibrary.readexcel(filename);
	}
	/*@DataProvider(name="createData")
	public Object[][] fetchData()
	{ Object[][] data = new Object[2][3];
		data[0][0] = "rgbs";
		data[0][1] = "radha";
		data[0][2] = "M";
	
		
		data[1][0] = "TL";
		data[1][1] = "vinu";
		data[1][2] = "F";
		return data;
		
		
	}*/
	
	//@Test(invocationCount=2) 
	@Test(groups="smoke",dataProvider ="createData")

	public void createlead(String cName ,String firstName, String lastName)
	{
		// Click Leads tab
		WebElement eleLeadstab = locateElement("link","Leads");
		eleLeadstab.click();
		// Click Create Lead Section
		WebElement eleCreatleadLink = locateElement("link", "Create Lead");
		click(eleCreatleadLink);
		// Enter Company Name
		WebElement eleCompanyName = locateElement("id", "createLeadForm_companyName");
		clearAndType(eleCompanyName, cName);
		// Enter First Name
		WebElement eleFirstName = locateElement("id","createLeadForm_firstName");
		clearAndType(eleFirstName,firstName);
		// Enter Last Name
		WebElement eleLastName = locateElement("id","createLeadForm_lastName");
		clearAndType(eleLastName,lastName);
		// dropdown selection for Source field using index
		WebElement ele2 = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingIndex(ele2,4);
		// dropdown selection for Industry field using attribute value
		WebElement ele3 = locateElement("id", "createLeadForm_industryEnumId");
		selectDropDownUsingValue(ele3,"IND_TELECOM");
		// dropdown selection for ownership field using visible text
		WebElement ele = locateElement("id", "createLeadForm_ownershipEnumId");
		selectDropDownUsingText(ele,"Corporation");
		// Creating a Lead using create lead button
		WebElement eleCreateLeadButton = locateElement("xpath","//input[@value='Create Lead']");
		eleCreateLeadButton.click();

	}

}
