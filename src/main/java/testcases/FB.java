package testcases;



	import java.util.HashMap;
	import java.util.Map;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.chrome.ChromeDriver;
	import org.openqa.selenium.chrome.ChromeOptions;
	import org.testng.annotations.BeforeClass;
	import org.testng.annotations.Test;

import com.yalla.selenium.api.base.Annotations;

	public class FB extends Annotations {
		@BeforeClass
		public void testDetails(){
			testcaseName="Facebook";
			testcaseDec="Like TestLeaf page in Facebook";
			author="radha";
			category= "Exercise";
		}
		
		@Test
		public void fb() throws InterruptedException {
			startApp("chrome", "https://facebook.com");
			WebElement eleUserId = locateElement("id", "email");
			clearAndType(eleUserId, "vinubabu34@yahoo.co.in");
			WebElement elePassword = locateElement("xpath", "//input[@id='pass']");
			clearAndType(elePassword, "godslovecare2128@86");
			WebElement eleLoginBtn = locateElement("xpath", "//input[@value='Log In']");
			click(eleLoginBtn);
			Thread.sleep(5000);
			WebElement eleSearchTextBox = locateElement("xpath", "//input[@data-testid='search_input']");
			clearAndType(eleSearchTextBox, "TestLeaf");
			WebElement eleSearchButton = locateElement("xpath", "//button[@data-testid='facebar_search_button']/i");
			click(eleSearchButton);
			Thread.sleep(5000);
			WebElement elePlaces = locateElement("xpath", "(//div[text()='Places'])[1]");
			click(elePlaces);
			WebElement eleLikeTestLeaf = locateElement("xpath", "(//a[@href='https://www.facebook.com/TestleafSolutionsIncChennai/']/preceding::button[@type='submit'])[3]");
			String eleLikeText = getElementText(eleLikeTestLeaf);
			if(eleLikeText.equalsIgnoreCase("Like")) {		
				click(eleLikeTestLeaf);
			}
			WebElement eleTestLeaf = locateElement("xpath", "//a[@href='https://www.facebook.com/TestleafSolutionsIncChennai/']");
			click(eleTestLeaf);
			Thread.sleep(2000);
			verifyTitle("TestLeaf");
			WebElement eleNoOfLike = locateElement("Xpath", "//div[contains(text(),'people like this')]");
			String likeText = getElementText(eleNoOfLike);
			String[] split = likeText.split(" ");
			String NoOfLikes=split[0];
			System.out.println("No of likes="+NoOfLikes);
			
			
		}
	

		
	}


